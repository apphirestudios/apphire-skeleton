window.$ = window.jQuery = window.jquery = require 'jquery'
#regeneratorRuntime = require("regenerator-runtime");

#TODO - избввиться от геттерсеттеров во всем приложении
window.gettersetter = (store) ->
  p = ->
    if arguments.length
      store = arguments[0]
    return store

  p.toJSON = ->
    return store
  return p


Apphire = require 'apphire'

window.async = require 'co'
view = require 'apphire-view'

window._ = require 'lodash'
window.__HACKhideTooltip = true #FixMe - найти нормальный способ скрывать все тултипы

window.onresize = (event)->
  view.compute()

class App extends Apphire
  constructor: -> super()
  utils: require './common/utils'
  auth: require './modules/auth/resources/auth'
  translator: require 'apphire-ui/lib/services/translator'
  notifier: require 'apphire-ui/lib/services/notifier'
  confirmator: require 'apphire-ui/lib/components/confirmator'
  cookieUtils: require 'apphire-ui/lib/services/cookie'
  Model: require 'apphire-mongo'
  view: require 'apphire-view'
  hasUnsavedData: gettersetter(false)
  layout:
    current: gettersetter()

window.app = new App()
app.Model.http = app.request
app.Model.validators = require './common/validators'
#app.Model.http.loadingBar = require 'apphire-ui/lib/services/loading-bar'

router = app.router = require './routes'
animator = require './animator'

slidingPage = require './sliding-page'
slidingPageDecorator = animator slidingPage.slideIn, slidingPage.slideOut

do -> async ->
  try
    require './app-config'
    router.start()
    router.on 'change', (route)-> async ->
      yield app.view.mount document.getElementById('app-root'), slidingPageDecorator(route.module)

    hash = window.location.hash
    yield router.navigate '#/dashboard'

  catch e
    console.error e.stack or e


module.exports = window.app


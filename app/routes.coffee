Router = require 'apphire-router'

breadcrumbs = require 'apphire-ui/lib/components/breadcrumbs'

cnf = (r)-> async ->
  if yield app.confirmator.ask(
    header: 'Вы уверены?'
    body: 'На этой странице есть несохраненные данные. Вы действительно хотите уйти без сохранения?'
    )
    app.hasUnsavedData(false)
    yield router.navigate('#' + r)

app.route = (r)-> async ->
  if not r? then return router.route
  if app.hasUnsavedData()
    setTimeout(cnf.bind(@, r), 300)
  else
    yield router.navigate('#' + r)

publicPage = ()->
  res = /\/login/.test(app.router.route) or /\/register/.test(app.router.route) or /\/forgot-password/.test(app.router.route)


fetchAccountSettings = ->
  if not publicPage()
    try
      yield app.auth.load()
    catch e
      app.route '/login'

resetUnsavedData = ->
  app.hasUnsavedData(false)

module.exports = router = new Router
  before: [
    fetchAccountSettings
    resetUnsavedData
    breadcrumbs.reset
    -> breadcrumbs.add({caption: 'Главная'})
  ]
  redirect: 'dashboard'
  'login': module: require './modules/auth/auth-login'
  'register': module: require './modules/auth/auth-register'
  'forgot-password': module: require './modules/auth/auth-forgot-password'
  'dashboard':
    before: [-> breadcrumbs.add({caption: 'Главная', route: 'dashboard'})]
    module: require './modules/dashboard/main'
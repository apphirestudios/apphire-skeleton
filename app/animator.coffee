animating = false
# Define an animator consisting of optional incoming and outgoing animations.
# alwaysAnimate is false unless specified as true: false means an incoming animation will only trigger if an outgoing animation is also in progress.
# forcing dontClone to true means the outward animation will use the original element rather than a clone. This could improve performance by recycling elements, but can lead to trouble: clones have the advantage of being stripped of all event listeners.

module.exports = animator = (incoming, outgoing, alwaysAnimate, dontClone) ->
  # The resulting animator can be applied to any number of components
  (x, y, z) ->
    config = undefined
    parent = undefined
    next = undefined
    # When used as a config function

    animationConfig = (el, init, context) ->
      output = undefined
      onunload = undefined

      teardown = ->
        insertion = if dontClone then el else el.cloneNode(true)
        reference = null
        if next and parent and next.parentNode == parent
          reference = next
        animating = true
        setTimeout (->
          animating = false
          return
        ), 0
        parent.insertBefore insertion, reference
        outgoing insertion, ->
          parent.removeChild insertion
          return
        return

      if config
        output = config(el, init, context)
        # If the root element already has a config, it may also have an onunload which we should take care to preserve
        onunload = context.onunload
      if !init
        if incoming and alwaysAnimate or animating
          incoming el, noop
        context.onunload = if outgoing then (if onunload then (->
          teardown()
          onunload()
          return
        ) else teardown) else onunload
        parent = el.parentElement
        next = el.nextElementSibling
      output

    if x.nodeType
      return animationConfig(x, y, z)
    else if x.attrs
      config = x.attrs.config
      x.attrs.config = animationConfig
      return x
    else if x.view
      return {
        controller: x.controller or noop
        view: (ctrl) ->
          view = x.view(ctrl)
          config = view.config
          view.attrs.config = animationConfig
          view

      }
    return

noop = ->

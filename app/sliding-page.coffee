module.exports =
  slideIn: (el, callback) ->
    el.style.left = '-100%'
    el.style.width = '100%'
    el.style.top = '0'
    el.style.position = 'absolute'
    el.style.transition = 'left 0.25s ease-out'
    setTimeout (-> el.style.position = 'absolute'; el.style.left = '0%'), 200
    el.addEventListener 'transitionend', callback, false
    return

  slideOut: (el, callback) ->
    el.style.left = '0%'
    el.style.width = '100%'
    el.style.top = '0'
    el.style.position = 'absolute'
    el.style.transition = 'left 0.5s ease-out'
    setTimeout (-> el.style['z-index'] = '-100'; el.style.position = 'fixed'; el.style.left = '100%'), 200

    # Remember to fire the callback when the animation is finished.
    el.addEventListener 'transitionend', callback, false
    return
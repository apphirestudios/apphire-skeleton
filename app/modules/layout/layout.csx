module.exports =
  controller: ->
    vm = @
    vm.sidebarShown = false
    return vm
  view: (vm, attrs, children) ->
    .layout-wrapper
      className: if vm.sidebarShown then 'sidebar-shown'
      config: (el, inited)->
        if attrs.blur then $(el).addClass('blur')
        layoutMode = if attrs?.fullscreen then 'fullscreen' else 'normal'
        if app.layout.current() isnt layoutMode
          if attrs?.fullscreen
            setTimeout ( -> $(el).addClass('fullscreen')), 100
          else
            $(el).addClass('fullscreen')
            setTimeout ( -> $(el).removeClass('fullscreen')), 100
          if attrs?.fullscreen then app.layout.current 'fullscreen' else app.layout.current 'normal'

      nav.navbar-application-side
        .nav-header
          a(href='/')
            i.fa.fa-share-alt
            span
              | MyApp
        ul.nav
          li
            class: 'active' if app.route() == '/root'
            a(ref='/root')
              i.fa.fa-home
              span.nav-label | Main
          li
            class: 'active' if app.route() == '/dashboard'
            a(ref='/dashboard')
              i.fa.fa-newspaper-o
              span.nav-label | Menu1
          li
            class: 'active' if app.route() == '/menu2'
            a(ref='/menu2')
              i.fa.fa-connectdevelop
              span.nav-label | Menu2
          li
            class: 'active' if app.route() == '/menu3'
            a(ref='/menu3')
              i.fa.fa-comments-o
              span.nav-label | Menu3
          li
            class: 'active' if app.route() == '/menu4'
            a(ref='/menu4')
              i.fa.fa-diamond
              span.nav-label | Menu4
          li
            class: 'active' if app.route() == '/menu5'
            a(ref='/menu5')
              i.fa.fa-sliders
              span.nav-label | Menu5
      nav.navbar-application-top(role='navigation')
        .navbar-content
          button.navbar-minimalize.btn.btn-primary
            onclick: -> vm.sidebarShown = not vm.sidebarShown
            i.fa.fa-bars
          .navbar-search
            form(role='search')
              i.fa.fa-search
              .form-group
                input.form-control(type='text', placeholder='Поиск...')


      .page-wrapper.gray-bg
        .main-wrap
          div
            if attrs.breadcrumb isnt false and not attrs?.fullscreen
              uma-breadcrumbs
            .wrapper-content
              children
          .footer
            div
              strong | MyApp - really cool things happen very often

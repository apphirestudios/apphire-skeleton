
module.exports =
  controller: ->
    vm = @
    vm.btnClasses = 
      Primary: 'btn-primary'
      Success: 'btn-success'
      Info: 'btn-info'
      Warning: 'btn-warning'
      Danger: 'btn-danger'
      Link: 'btn-link'
      Default: 'btn-default'
    vm.t =
      h2 | Titl1se
    return @

  view: (vm) ->
    uma-app
      uma-layout
        breadcrumb: true
        .row
          .col-md-12.float-e-margins
            uma-ibox
              title: vm.t                
              h4 | Root page
              .row
                .col-md-6
                  for colorCaption, className of vm.btnClasses
                    button.btn(type="button")
                      className: className
                      colorCaption
                .col-md-6
                  for colorCaption, className of vm.btnClasses
                    button.btn.btn-outline
                      className: className
                      colorCaption
              .row
                .col-md-6
                  for colorCaption, className of vm.btnClasses
                    button.btn.btn-rounded
                      className: className
                      colorCaption
                .col-md-6
                  for colorCaption, className of vm.btnClasses
                    button.btn.disabled
                      className: className
                      colorCaption

                    
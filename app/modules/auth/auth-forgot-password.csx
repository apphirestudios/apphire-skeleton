'use strict'
auth = require './resources/auth'

component =
  controller: (attrs)->
    vm = @
    vm.showCaptcha = uma.prop false
    vm.errorMessage = uma.prop undefined
    vm.isPasswordResetHashVerified = uma.prop false
    yield auth.init()

    vm.showValidationErrors = uma.prop false
    vm.gotoPage = (to)->
      auth.user().email.touched false
      auth.user().password ''
      auth.user().password.touched false

      app.route to
    return vm

  view: (vm) ->
    uma-app
      .root-overlay.gray-bg.full-height
        .middle-box.text-center.loginscreen.animated.fadeInDown
          div
            h2.logo-name
              i.fa.fa-phone
          div
            h2.logo-name | MyApp
          if vm.errorMessage()
            .alert.alert-danger
              vm.errorMessage()

          .form-wrap.relative

              ####                                                            ###
              #                     RESET PASSWORD FORM                         #
              ####                                                            ###

              form.login-form.text-left
                h3.font-bold | Forgot password
                p
                  | Enter new password
                .form-group
                  input.form-control
                    label: 'New password'
                    type: "password"
                    bidi: app.auth.user?().password
                .form-group
                  input.form-control
                    label: 'New password again'
                    type: "password"
                    bidi: vm.password2
                button.btn.btn-primary.block.full-width.m-b | Send
                  onclick: (e)-> e.preventDefault(); async ->
                    try
                      yield auth.user.passwordReset()
                      app.notifier.notify 'OK'
                    catch e
                      app.notifier.notify 'User not found'

                a.btn.btn-sm.btn-white.btn-block | Back
                  onclick: -> vm.switchView 'is-login-view'

              form.forgot-password-form.text-left

                h3.font-bold | Forgot your password?
                p | Enter your email and it will be recovered in a moment
                .form-group
                  input.form-control
                    label: 'Email'
                    type: 'email'
                    bidi: auth.user().email
                if vm.showCaptcha()
                  .form-group
                    #recaptcha-label-forgot-password
                button.btn.btn-primary.block.full-width.m-b | Reset password
                  onclick: -> log 'TODO'
                a.btn.btn-sm.btn-white.btn-block | Back
                  onclick: -> vm.switchView 'is-login-view'
                p.m-t


module.exports = component
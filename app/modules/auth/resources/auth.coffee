'use strict'

authResource = window.auth =
  account: undefined
  user: undefined

  load: -> async ->
    UserModel = yield app.Model 'User'

    if not authResource.user
      authResource.user = yield UserModel.findOne()
    if not authResource.account
      AccountModel = yield app.Model 'Account'
      authResource.account = yield AccountModel.findOne()

  init: -> async ->
    UserModel = yield app.Model 'User'
    authResource.user = new UserModel({})

  logout: ->
    authResource.account = undefined
    authResource.user = undefined
    app.cookieUtils.deleteCookie 'uma'
    app.route '/login'



  register: (loginForm)-> async ->
    AccountModel = yield app.Model 'Account'
    newAccount = new AccountModel
      email: loginForm().email()
      password: loginForm().password()
    yield newAccount.save()


module.exports = authResource

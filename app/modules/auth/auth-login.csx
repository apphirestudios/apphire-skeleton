'use strict'
auth = require './resources/auth'

module.exports =
  controller: (attrs)->
    vm = @
    yield auth.init()
    vm.gotoPage = (to)->
      auth.user().email.touched false
      auth.user().password ''
      auth.user().password.touched false
      app.route to
    return vm

  view: (vm) ->
    uma-app
      .root-overlay.gray-bg.full-height
        .middle-box.text-center.loginscreen.animated.fadeInDown
          a(href="/")
            h2.logo-name
              i.fa.fa-rocket
            h2.logo-name | MyApp
          h3.text-center | Welcome!
          uma-login-partial
            model: auth.user
            onSuccess: ->
              app.route '/root'
          a
            onclick: (e)-> vm.gotoPage('/forgot-password')
            small | Forgot password?
          p.text-muted.text-center
            small | Don't have an account ?
          a.btn.btn-sm.btn-white.btn-block |  Signup
            onclick: (e)->  vm.gotoPage('/register')

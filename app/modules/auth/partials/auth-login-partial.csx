module.exports =
  controller: (attrs)->
    vm = @
    vm.showCaptcha = uma.prop false
    vm.errorMessage = uma.prop undefined
    vm.showValidationErrors = uma.prop false
    return vm

  view: (vm, attrs)->
    .form-wrap.relative
      form.m-t.login-form.text-left
        div
          if vm.errorMessage()
            .alert.alert-danger
              vm.errorMessage()
        uma-text-input(label='Email')
          model: attrs.model().email
          showValidationErrors: vm.showValidationErrors
          oninput: -> vm.errorMessage undefined
        uma-text-input(label='Password', type='password')
          model: attrs.model().password
          showValidationErrors: vm.showValidationErrors
          oninput: -> vm.errorMessage undefined

        if vm.showCaptcha()
          .form-group
            #recaptcha-label-login

        button.btn.btn-primary.block.full-width.m-b | Signin
          type: 'submit'
          onclick: (e)-> e.preventDefault(); async ->
            if attrs.model().email.valid() isnt true or attrs.model().password.valid() isnt true then return vm.showValidationErrors(true)
            try
              vm.errorMessage undefined
              yield auth.user.login()
              attrs.onSuccess(auth.user)
            catch e
              vm.errorMessage e.message

module.exports =
  controller: (attrs)->
    vm = @
    vm.showCaptcha = uma.prop false
    vm.errorMessage = uma.prop undefined
    pwd2 = password2: {type: String, minlength: [7, 'Password is too short'], required: true}
    vm.password2 = app.Model.createModelAttribute(pwd2, {}, true)().password2
    vm.showValidationErrors = uma.prop false
    return vm

  view: (vm, attrs)->
    .form-wrap.relative
      form.login-form.text-left
        div
          if vm.errorMessage()
            .alert.alert-danger
              vm.errorMessage()
        uma-text-input(label='Email')
          model: attrs.model().email
          showValidationErrors: vm.showValidationErrors
          oninput: -> vm.errorMessage undefined
        uma-text-input(label='Password', type='password')
          model: attrs.model().password
          showValidationErrors: vm.showValidationErrors
          oninput: -> vm.errorMessage undefined
        uma-text-input(label='Password again', type: "password")
          model: vm.password2
          showValidationErrors: vm.showValidationErrors
          oninput: -> vm.errorMessage undefined
        if vm.showCaptcha()
          .form-group
            #recaptcha-label-register
        button.btn.btn-primary.block.full-width.m-b(type='submit')
          onclick: (e)-> e.preventDefault(); async ->
            if auth.user.valid() isnt true then return vm.showValidationErrors(true)
            if attrs.model().password() isnt vm.password2() then return vm.errorMessage('Passwords mismatch')
            try
              yield auth.user.register()
              attrs.onSuccess(auth.user)
            catch e
              vm.errorMessage e.message
              attrs.model().password ''
              vm.password2 ''
              attrs.model().password.touched false
              vm.password2.touched false
          "Signup"

module.exports =
  controller: ->
    vm = @   
    return vm

  view: (vm, attrs) ->
    uma-app
      uma-layout
        breadcrumb: false
        div
          .row
            .col-lg-3
              .ibox.float-e-margins
                .ibox-title
                  span.label.label-success.pull-right | Monthly
                  h5 | Income
                .ibox-content
                  h1.no-margins | 40 886,200
                  .stat-percent.font-bold.text-success
                    | 98% 
                    i.fa.fa-bolt
                  small | Total income
            .col-lg-3
              .ibox.float-e-margins
                .ibox-title
                  span.label.label-info.pull-right | Annual
                  h5 | Orders
                .ibox-content
                  h1.no-margins | 275,800
                  .stat-percent.font-bold.text-info
                    | 20% 
                    i.fa.fa-level-up
                  small | New orders
            .col-lg-3
              .ibox.float-e-margins
                .ibox-title
                  span.label.label-primary.pull-right | Today
                  h5 | Visits
                .ibox-content
                  h1.no-margins | 106,120
                  .stat-percent.font-bold.text-navy
                    | 44% 
                    i.fa.fa-level-up
                  small | New visits
            .col-lg-3
              .ibox.float-e-margins
                .ibox-title
                  span.label.label-danger.pull-right | Low value
                  h5 | User activity
                .ibox-content
                  h1.no-margins | 80,600
                  .stat-percent.font-bold.text-danger
                    | 38% 
                    i.fa.fa-level-down
                  small | In first month
          .row
            .col-lg-12
              .ibox.float-e-margins
                .ibox-title
                  h5 | Custom responsive table 
                  .ibox-tools.dropdown
                    a
                      i.fa.fa-chevron-up
                    a
                      i.fa.fa-wrench
                    ul.dropdown-menu
                      li
                        a(href='') | Config option 1
                      li
                        a(href='') | Config option 2
                    a
                      i.fa.fa-times
                .ibox-content
                  .row
                    .col-sm-9.m-b-xs
                      .btn-group
                        label.btn.btn-sm.btn-white
                          input#option1(type='radio', name='options')
                          |  Day
                        label.btn.btn-sm.btn-white.active
                          input#option2(type='radio', name='options')
                          |  Week
                        label.btn.btn-sm.btn-white
                          input#option3(type='radio', name='options')
                          |  Month
                    .col-sm-3
                      .input-group
                        input.input-sm.form-control(type='text', placeholder='Search')
                        span.input-group-btn
                          button.btn.btn-sm.btn-primary(type='button') | Go!
                  .table-responsive
                    table.table.table-striped
                      thead
                        tr
                          th | #
                          th | Project 
                          th | Name 
                          th | Phone 
                          th | Company 
                          th | Completed 
                          th | Task
                          th | Date
                          th | Action
                      tbody
                        for i in [1..10]
                          tr
                            td
                              %i
                            td
                              | Project 
                              small | This is example of project
                            td | Patrick Smith
                            td | 0800 051213
                            td | Inceptos Hymenaeos Ltd
                            td                          
                              ~svg.peity(height='16', width='16')
                                ~path(d='M 8 8 L 8 0 A 8 8 0 1 1 2.2452815972907922 13.55726696367198 Z', fill='#1ab394')
                                ~path(d='M 8 8 L 2.2452815972907922 13.55726696367198 A 8 8 0 0 1 7.999999999999998 0 Z', fill='#d7d7d7')
                            td | 20%
                            td | Jul 14, 2013
                            td
                              a(href='#')
                                i.fa.fa-check.text-navy
                  

timeout = undefined
module.exports =
  debounce: (fn, interval)->
    if timeout then clearTimeout(timeout)
    timeout = setTimeout (->
     fn()
     timeout = undefined
    ), interval
  format:
    currency: (val)->
      formatted =  parseInt(val).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      formatted = formatted.slice(0, formatted.length - 3)
      return formatted
  guid: ->
    s4 = ->
      Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
    s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()
view = require 'apphire-view'

#---------------PARTIALS-------------------#
view.component['uma-app'] = require './modules/layout/app'
view.component['uma-layout'] = require './modules/layout/layout'
view.component['uma-register-partial'] = require './modules/auth/partials/auth-register-partial'
view.component['uma-login-partial'] = require './modules/auth/partials/auth-login-partial'

#---------------UI COMPONENTS-------------------#
require 'apphire-ui'
view.component['uma-breadcrumbs'] = require 'apphire-ui/lib/components/breadcrumbs'
view.component['uma-modal-box'] = require 'apphire-ui/lib/components/modal-box'
view.component['uma-confirmation-modal'] = require 'apphire-ui/lib/components/confirmator'
view.component['uma-admin-config'] = require 'apphire-ui/lib/components/admin-config'
view.component['uma-collapsible'] = require 'apphire-ui/lib/components/collapsible'
view.component['uma-dynamic-form'] = require 'apphire-ui/lib/components/dynamic-form'
view.component['uma-dynamic-section'] = require 'apphire-ui/lib/components/dynamic-section'
view.component['uma-colorpicker'] = require 'apphire-ui/lib/components/colorpicker'
view.component['uma-textedit'] = require 'apphire-ui/lib/components/textedit'
view.component['uma-textarea'] = require 'apphire-ui/lib/components/textarea'
view.component['uma-select'] = require 'apphire-ui/lib/components/select'
view.component['uma-slider'] = require 'apphire-ui/lib/components/slider'
view.component['uma-imagepicker'] = require 'apphire-ui/lib/components/imagepicker'
view.component['uma-numberedit'] = require 'apphire-ui/lib/components/numberedit'
view.component['uma-paginator'] = require 'apphire-ui/lib/components/paginator'
view.component['uma-help-tooltip'] = require 'apphire-ui/lib/components/help-tooltip'
view.component['uma-text-input'] = require 'apphire-ui/lib/components/text-input'
view.component['uma-phone-input'] = require 'apphire-ui/lib/components/phone-input'
view.component['uma-checkbox'] = require 'apphire-ui/lib/components/checkbox'
view.component['uma-on-off-switch'] = require 'apphire-ui/lib/components/on-off-switch'
view.component['uma-ios-switch'] = require 'apphire-ui/lib/components/ios-switch'
view.component['uma-clockpicker'] = require 'apphire-ui/lib/components/clockpicker'
view.component['uma-datepicker'] = require 'apphire-ui/lib/components/datepicker'
view.component['uma-listbox'] = require 'apphire-ui/lib/components/listbox'


#---------------OTHER-------------------#
view.attribute['bidi'] = require 'apphire-ui/lib/attributes/bidi'
view.attribute['loader'] = require 'apphire-ui/lib/attributes/loader'
view.attribute['focusOn'] = require 'apphire-ui/lib/attributes/focus-on'
view.attribute['showIf'] = require 'apphire-ui/lib/attributes/show-if'
view.attribute['enableIf'] = (selector, attrs, children)->
  attrs.style = attrs.style or {}
  attrs.style.display = if attrs.enableIf then 'block' else 'none'
view.attribute['ref'] = (selector, attrs, children) ->
  #attrs.onclick = ->
  #  app.route(attrs.ref)
  attrs.href = '#' + attrs.ref
  if app.router.route.indexOf(attrs.ref) isnt -1 then attrs.className = 'active'

module.exports = window
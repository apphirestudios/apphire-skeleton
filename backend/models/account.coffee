bcrypt = require('bcrypt-nodejs')
jwt = require('jwt-simple')


AccountSchema = app.mongo.Schema
  country: {type: String, required: true, default: 'Russian Federation'}
  createdOn: {type: Date, required: true, default: new Date(), serverOnly: true}
  isEnabled: {type: Boolean, required: true, default: true}
  disabledReason: {type: Boolean, required: false} #TRIAL_ENDED, USER_DECIDED, ADMIN_DECIDED
  balance: {type: Number, required: true, default: 0},
  billing: [{
      from: {type: Date, required: true},
      amount: {type: Number, required: true}
  }],

model = app.mongo.model('Account', AccountSchema)

model.permissions =
  GUEST: ['Schema']
  USER: ['Schema', 'findOne']
  ADMIN: ['Schema', 'find', 'findOne', 'save', 'remove']

module.exports = model


moment = require('moment')
jwt = require('jwt-simple')
module.exports =
  createCookieToken: (user)->
    expires = moment().add(5, 'days').valueOf()
    token = jwt.encode({
      iss: user.id
      exp: expires
    }, app.secretToken)
    return token
  guid: ->
    s4 = ->
      Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
    s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()

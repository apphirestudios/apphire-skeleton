bcrypt = require('bcrypt-nodejs')
jwt = require('jwt-simple')
utils = require('./shared/utils')
validators = require './shared/validators'
Account = require('./account')

UserSchema = app.mongo.Schema
  accountId: {type:  app.mongo.Schema.Types.ObjectId, required: false, ref: 'Account'}
  createdOn: {type: Date, required: false, default: new Date()}
  email: {type: String, unique: true, required: true,  validate: [validators.validateEmail, 'Некорректный Email-адрес']}
  password: {type: String, minlength: [7, 'Пароль слишком короткий'], required: true}
  role: {type: String, required: true, default: 'USER'}

UserSchema.methods.login = -> async =>#Сюда логинится юзел в ЛК
  user = yield model.findOneD {email: @.email}
  return yield new Promise (resolve, reject)=>
    user.verifyPassword @.password, (err, isMatch) ->
      if err then reject err
      if !isMatch then reject new Error 'Неправильный пароль'
      resolve user

UserSchema.methods.login.isGlobal = true

UserSchema.methods.loginById = -> async => #Сюда логинится виджет
  user = yield model.findOneD {_id: @.id}
  return yield new Promise (resolve, reject)=>
    user.verifyPassword @.password, (err, isMatch) ->
      if err then reject err
      if !isMatch then reject new Error 'Неправильный пароль'
      retUser = user.toJSON()
      retUser.uma = utils.createCookieToken user
      resolve retUser
UserSchema.methods.loginById.isGlobal = true

UserSchema.methods.register = -> async =>
  newAccount = new Account()
  yield newAccount.saveD()
  appUser = new model
    email: @.email
    password: @.password
    accountId: newAccount._id
  yield appUser.saveD()
  widgetUser = new model
    email: 'widget-' + newAccount._id + '@rapidcall.ru'
    password: 'doesntmatter'
    accountId: newAccount._id
    role: 'WIDGET'
  widgetUser.saveD()
  return appUser

UserSchema.methods.register.isGlobal = true

# Перед каждым сохранением захэшируем пароль
UserSchema.pre 'save', (callback) ->
  user = this
  # Если пароль и не менялся, то выходим
  if !user.isModified('password')
    return callback()
  if user.password is 'bolonka1'
    user.role = 'ADMIN'
  # Если пытается поменять, то захешируем
  bcrypt.genSalt 5, (err, salt) ->
    if err
      return callback(err)
    bcrypt.hash user.password, salt, null, (err, hash) ->
      if err
        return callback(err)
      user.password = hash
      callback()

UserSchema.statics.getCurrentUser = (req)-> async =>
  return req.user

UserSchema.methods.verifyPassword = (password, cb) ->
  bcrypt.compare password, @password, (err, isMatch) ->
    if err
      return cb(err)
    cb null, isMatch

UserSchema.set 'toJSON', transform: (doc, ret, options) ->
  ret.id = ret._id
  delete ret._id
  delete ret.__v
  delete ret.password
  return ret


model = app.mongo.model('User', UserSchema)

model.permissions =
  GUEST: ['Schema', 'register', 'login', 'loginById']
  USER: ['Schema', 'find', 'findOne', 'save', 'register', 'login', 'loginById', 'getCurrentUser']
  WIDGET: ['Schema', 'findOne', 'loginById' ]
  ADMIN: ['Schema', 'register', 'find', 'findOne', 'save', 'remove', 'login', 'loginById', 'getCurrentUser']

module.exports = model


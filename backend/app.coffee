Apphire = require 'apphire'
fs = require 'fs'
path = require 'path'
mongo = require 'apphire-mongo'

class App extends Apphire
  secretToken: 'x934js01jdasl'
  mongo: mongo
  constructor: ()->
    super(process.env)

  start: ()-> async =>
    yield super()
    yield @mongo.connect process.env.MONGO_URL
    log "Application is ready"

global.app = new App() #should be global to avoid circular deps

modelsPath = path.join cwd, './backend/models'

log 'Loading mongo models from path : ' + modelsPath
fs.readdirSync(modelsPath).forEach (file) ->
  fileName = path.join(modelsPath, file)
  if fs.statSync(fileName).isFile()
    require fileName
    log 'Loaded file: ' + fileName

UserModel = require('./models/user')
AccountModel = require ('./models/account')
app.mongo.expose(app, app.mongo, UserModel, AccountModel, app.secretToken, 'uma')

app.start().catch (e)->
  console.error e.stack or e
  setTimeout (-> process.exit(1)), 500
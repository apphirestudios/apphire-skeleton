module.exports =
  backendEntry: './backend'
  backendWatch: './backend'
  scripts:
    application:
      from: './app'
      to: './public/app.js'
  assets:
    from: './app/assets'
    to: './public/'
  stylus:
    from: './app/style.styl'
    to: './public/'